cmake_minimum_required ( VERSION 3.12 )
project ( reactive C )

set(CMAKE_VERBOSE_MAKEFILE ON)
set ( CMAKE_C_STANDARD 11 )

set ( observerSrc observer.c observer.h observer_types.h )
set ( observableSrc observable.c observable.h observable_types.h )
set ( timersSrc timers.c timers.h timers_types.h )

add_executable ( reactive main.c ${observerSrc} ${observableSrc} ${timersSrc} )
