#include "observable_types.h"

void Observable_create ( Observable_t * observable, void ( * creationFunction ) ( Observer_t * ) )
{
    observable -> creationFunction = creationFunction;
}

void Observable_subscribe ( Observable_t * observable, Observer_t * observer )
{
    if ( observable -> creationFunction )
        observable -> creationFunction ( observer );
}
