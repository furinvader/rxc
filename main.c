#include <stdio.h>
#include <stdint.h>
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "observer_types.h"
#include "observer.h"
#include "observable_types.h"
#include "observable.h"
#include "timers_types.h"
#include "timers.h"



struct Timestamp {
    time_t seconds;
    long milliseconds;
    char timestring[32];
};


struct Timestamp getTimestamp()
{
    char   timebuffer[32]     = {0};
    struct timeval  tv        = {0};
    struct tm      *tmval     = NULL;
    struct tm       gmtval    = {0};
    struct timespec curtime   = {0};

    struct Timestamp timestamp;

    int i = 0;

    // Get current time
    clock_gettime(CLOCK_REALTIME, &curtime);

    // Set the fields
    timestamp.seconds      = curtime.tv_sec;
    timestamp.milliseconds = round(curtime.tv_nsec/1.0e6);

    if((tmval = gmtime_r(&timestamp.seconds, &gmtval)) != NULL)
    {
        // Build the first part of the time
        strftime(timebuffer, sizeof timebuffer, "%Y-%m-%d %H:%M:%S", &gmtval);

        // Add the milliseconds part and build the time string
        snprintf(timestamp.timestring, sizeof timestamp.timestring, "%s.%03ld", timebuffer, timestamp.milliseconds);
    }

    return timestamp;
}



void afterTimeout ( void * parameter )
{
    Observer_t * observer = ( Observer_t * ) parameter;
    int x = * ( int * ) ( parameter + sizeof ( Observer_t ) );

    Observer_next ( observer, & x );
}

void creationFunction ( Observer_t * observer )
{
    int parameter1 = 1;
    Observer_next ( observer, & parameter1 );

    int parameter2 = 2;
    Observer_next ( observer, & parameter2 );

    int parameter3 = 3;
    Observer_next ( observer, & parameter3 );

    int parameter4 = 4;

    struct timespec start;
    clock_gettime ( CLOCK_PROCESS_CPUTIME_ID, & start );
    Timeout_t * timeout = malloc ( sizeof ( Timeout_t ) + sizeof ( Observer_t ) + sizeof ( int ) );
    timeout -> start = start;
    timeout -> time = 1000;
    timeout -> callback . function = afterTimeout;
    memcpy ( timeout -> callback . parameter, observer, sizeof ( Observer_t ) );
    memcpy ( timeout -> callback . parameter + sizeof ( Observer_t ), & parameter4, sizeof ( int ) );

    setInterval ( timeout );

    Observer_complete ( observer );
}

void next ( void * parameter )
{
    int x = * ( int * ) parameter;
    struct Timestamp timestamp = getTimestamp ();

    printf ( "%s - got value %d\n", timestamp.timestring, x );
}

void error ( void * parameter )
{
    int x = * ( int * ) parameter;

    printf ( "something wrong occured: %d\n", x );
}

void complete ( void )
{
    printf ( "done\n" );
}

int main ()
{
    Observable_t observable;
    Observable_create ( & observable, creationFunction );

    struct Timestamp timestamp = getTimestamp ();
    printf ( "%s - Just before subscribe\n", timestamp.timestring );

    Observer_t observer = { next, error, complete };
    Observable_subscribe ( & observable, & observer );

    printf ( "Just after subscribe\n" );

    sleep ( 5 );
}
