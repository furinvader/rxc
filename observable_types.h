#pragma once

#include "observer_types.h"

typedef struct
{
    void ( * creationFunction ) ( Observer_t * observer );
} Observable_t;
