#pragma once

typedef struct
{
    void ( * next ) ( void * parameter );
    void ( * error ) ( void * parameter );
    void ( * complete ) ( void );
} Observer_t;

typedef struct
{
    Observer_t * observer;
    void * parameter;
} ObserverParameter_t;
