#include "observer_types.h"

void Observer_next ( Observer_t * observer, void * parameter1 )
{
    if ( observer -> next )
        observer -> next ( parameter1 );
}

void Observer_complete ( Observer_t * observer )
{
    if ( observer -> complete )
        observer -> complete ();
}
