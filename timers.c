#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <cxxabi.h>
#include <math.h>

#include "timers_types.h"

typedef struct
{
    uint_fast64_t interval;
} interval_t;

void * runLoopAsThread ( void * parameter )
{
    Timeout_t * timeout = parameter;

    struct timespec start, stop;
    start = timeout -> start;

    while (1)
    {
        clock_gettime ( CLOCK_PROCESS_CPUTIME_ID, & stop );

        useconds_t diff = (useconds_t) ( ( stop . tv_sec - start . tv_sec ) * 1e6 + ( stop . tv_nsec - start . tv_nsec ) / 1e3 );    // in microseconds

        if ( diff < timeout -> time )
            usleep ( ( timeout -> time * 1000 - diff ) );

        clock_gettime ( CLOCK_PROCESS_CPUTIME_ID, & start );

        void ( * callback ) ( void * ) = timeout -> callback . function;
        void * callbackParameter = timeout -> callback . parameter;

        callback ( callbackParameter );
    }

    free ( parameter );
    return NULL;
}

void every ( uint_fast64_t intervalInMs, void ( * function ) ( void ) )
{
    pthread_t tid;
    pthread_create ( & tid, NULL, runLoopAsThread, NULL);
}

void setInterval ( Timeout_t * timeout )
{
    pthread_t tid;
    pthread_create ( & tid, NULL, runLoopAsThread, timeout);
}

void * runAsThread ( void * parameter )
{
    Timeout_t * timeout = parameter;

    struct timespec start, stop;
    start = timeout -> start;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);

    useconds_t diff = ( useconds_t ) ( (stop.tv_sec - start.tv_sec) * 1e6 + (stop.tv_nsec - start.tv_nsec) / 1e3 );    // in microseconds

    if ( diff < timeout -> time )
        usleep ( ( timeout -> time * 1000 - diff ) );

    /// do something
    void ( * callback ) ( void * ) = timeout -> callback . function;
    void * callbackParameter =  timeout -> callback . parameter;

    callback ( callbackParameter );

    free ( parameter );
    return NULL;
}

void setTimeout ( Timeout_t * timeout )
{
    pthread_t tid;
    pthread_create ( & tid, NULL, runAsThread, timeout );
}