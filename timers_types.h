#pragma once

#include <stdint.h>
#include <time.h>

typedef struct
{
    void * function;
    uint8_t parameter [ 0 ];
} Callback_t;

typedef struct
{
    struct timespec start;
    uint32_t time;
    Callback_t callback;
} Timeout_t;
