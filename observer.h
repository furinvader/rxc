#pragma once

#include "observer_types.h"

void Observer_next ( Observer_t * observer, void * parameter1 );
void Observer_complete ( Observer_t * observer );