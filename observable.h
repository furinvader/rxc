#pragma once

#include "observer_types.h"
#include "observable_types.h"

void Observable_create ( Observable_t * observable, void ( * creationFunction ) ( Observer_t * observer ) );
void Observable_subscribe ( Observable_t * observable, Observer_t * observer );
